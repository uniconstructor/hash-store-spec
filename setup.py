# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

setup(
    name='hashstorage',
    version='0.1.0',
    description='',
    long_description=readme,
    #author='',
    #author_email='',
    url='https://example.com',
    # license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)