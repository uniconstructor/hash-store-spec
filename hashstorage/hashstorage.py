from hashlib import sha256
# https://docs.python.org/3/library/typing.html
from typing import Any, List, Dict, Optional, Type, TypeVar, Text, Generic, Literal, Union
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, ConstBitStream
# https://docs.python.org/3/library/itertools.html
from itertools import count, accumulate, dropwhile, takewhile, filterfalse, tee, islice
import collections
import logging
import math
from .utils import bitLen, positionToNounce, toLocalPosition

HASH_SIZE_BYTES = sha256().digest_size
HASH_SIZE_BITS  = HASH_SIZE_BYTES * 8
# position encoding defaults
ITEM_HEADER_POSITION_BITS = 4
ITEM_HEADER_VALUE_BITS    = 4
ITEM_HEADER_LENGTH_BITS   = ITEM_HEADER_POSITION_BITS + ITEM_HEADER_VALUE_BITS
ITEM_HEADER_LENGTH_BYTES  = math.ceil((ITEM_HEADER_LENGTH_BITS) / 8)
MIN_POSITION_LENGTH_BITS  = 8
MIN_POSITION_LENGTH_BYTES = math.ceil((MIN_POSITION_LENGTH_BITS) / 8)
MAX_POSITION_LENGTH_BITS  = MIN_POSITION_LENGTH_BITS + ((2 ** ITEM_HEADER_POSITION_BITS) - 1)
MAX_POSITION_LENGTH_BYTES = math.ceil((MAX_POSITION_LENGTH_BITS) / 8)
MIN_VALUE_LENGTH_BITS     = ITEM_HEADER_LENGTH_BITS + MIN_POSITION_LENGTH_BITS
MIN_VALUE_LENGTH_BYTES    = math.ceil((MIN_VALUE_LENGTH_BITS) / 8)
MIN_EXTRA_POSITION_VALUE  = 0
MAX_EXTRA_POSITION_VALUE  = (2 ** (MIN_POSITION_LENGTH_BITS + (2 ** ITEM_HEADER_VALUE_BITS) - 1)) - 1
MIN_EXTRA_LENGTH_VALUE    = 0
MAX_EXTRA_LENGTH_VALUE    = (2 ** (2 ** ITEM_HEADER_VALUE_BITS) - 1) - 1

# https://docs.python.org/3/library/itertools.html#itertools-recipes
def slidingWindow(iterable, n):
    """
    >>> [item for item in slidingWindow('ABCDEFG', 4)]
    [('A', 'B', 'C', 'D'), ('B', 'C', 'D', 'E'), ('C', 'D', 'E', 'F'), ('D', 'E', 'F', 'G')]
    """
    it = iter(iterable)
    window = collections.deque(islice(it, n), maxlen=n)
    if len(window) == n:
        yield tuple(window)
    for x in it:
        window.append(x)
        yield tuple(window)

# # https://docs.python.org/3/library/itertools.html#itertools-recipes
def uniqueEverseen(iterable, key=None):
    """ List unique elements, preserving order. Remember all elements ever seen.
    >>> [item for item in uniqueEverseen('AAAABBBCCDAABBB')]
    ['A', 'B', 'C', 'D']
    >>> [item for item in uniqueEverseen(slidingWindow('ABCDEFGABCAAA', 4))]
    [('A', 'B', 'C', 'D'), ('B', 'C', 'D', 'E'), ('C', 'D', 'E', 'F'), ('D', 'E', 'F', 'G'), ('E', 'F', 'G', 'A'), ('F', 'G', 'A', 'B'), ('G', 'A', 'B', 'C'), ('A', 'B', 'C', 'A'), ('B', 'C', 'A', 'A'), ('C', 'A', 'A', 'A')]
    """
    seen = set()
    seen_add = seen.add
    if key is None:
        for element in filterfalse(seen.__contains__, iterable):
            seen_add(element)
            yield element
    else:
        for element in iterable:
            k = key(element)
            if k not in seen:
                seen_add(k)
                yield element

def hashFromBytes(bytesData: bytes) -> bytes:
    """
    >>> hashFromBytes(bytes(0)).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> hashFromNumber(0).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    """
    return sha256(bytesData).digest()

def hashFromNumber(number: int) -> bytes:
    """
    >>> hashFromNumber(0).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> hashFromBytes(bytes(0)).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> hashFromNumber(1).hex()
    '6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    >>> hashFromBytes(bytes(1)).hex()
    '6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    """
    return hashFromBytes(bytes(number))

def hashesFromNumbers(numbers: "list[int]") -> bytes:
    """
    >>> listHashes  = hashesFromNumbers([0, 1])
    >>> rangeHashes = hashesFromNumbers(range(0, 2))
    >>> next(listHashes).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> next(rangeHashes).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> next(listHashes).hex()
    '6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    >>> next(rangeHashes).hex()
    '6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    >>> ''.join([byte.hex() for byte in hashesFromNumbers([0, 1])])
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b8556e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    >>> ''.join([byte.hex() for byte in hashesFromNumbers([0, 2])])
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b85596a296d224f285c67bee93c30f8a309157f0daa35dc5b87e410b78630a09cfc7'
    """ 
    for i in numbers:
        yield hashFromNumber(i)

def createHashRange(startNounce: int, endNounce: int, step: Optional[int]=1) -> bytearray:
    """ 
    >>> createHashRange(0, 1).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> len(createHashRange(0, 1))
    32
    >>> createHashRange(0, 2).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b8556e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    >>> len(createHashRange(0, 2))
    64
    >>> createHashRange(1, 2).hex()
    '6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    >>> createHashRange(1, 3).hex()
    '6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d96a296d224f285c67bee93c30f8a309157f0daa35dc5b87e410b78630a09cfc7'
    """
    return bytearray().join(hashesFromNumbers(range(startNounce, endNounce, step)))

def hashRangeFromPositions(startPosition: int, endPosition: int) -> bytes:
    """
    >>> hashRangeFromPositions(0, 1).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> hashRangeFromPositions(0, 31).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> hashRangeFromPositions(0, 32).hex()
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b8556e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    >>> hashRangeFromPositions(32, 63).hex()
    '6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    """
    startNounce = positionToNounce(startPosition)
    endNounce   = positionToNounce(endPosition) + 1
    return createHashRange(startNounce, endNounce)

def hashSlice(hashRange: bytes, start: int, end: int, step: Optional[int]=1) -> bytes:
    """
    >>> hashRange = createHashRange(0, 1)
    >>> next(hashSlice(hashRange, 0, 1)).hex()
    'e3'
    >>> [byte.hex() for byte in hashSlice(hashRange, 0, 3)]
    ['e3', 'b0', 'c4']
    >>> ''.join([byte.hex() for byte in hashSlice(hashRange, 0, 3)])
    'e3b0c4'
    """
    startPosition = toLocalPosition(start)
    endPosition   = toLocalPosition(end)
    position      = startPosition
    while position < endPosition:
        yield bytes([hashRange[position]])
        position += step

def dumpHashRange(startNounce: int, endNounce: int, pathTemplate='hashstorage/data/hash_{}-{}.txt') -> ConstBitStream:
    """
    >>> data = dumpHashRange(0, 2**8)
    >>> data
    ConstBitStream(filename='hashstorage/data/hash_0-256.txt', length=65536)
    >>> len(data.bytes)
    8192
    >>> dumpHashRange(2**21+1, 2**21+2**21)
    """
    hashes   = []
    fileName = pathTemplate.format(startNounce, endNounce)
    file     = open(fileName, 'wb')
    for nounce in range(startNounce, endNounce):
        hash = hashFromNumber(nounce)
        hashes.append(hash)
    file.write(bytearray().join(hashes))
    file.close()
    return ConstBitStream(filename=fileName)

def getEncoderKeyFormat(bitLength: int) -> str:
    """
    >>> getEncoderKeyFormat(5)
    'bin:5'
    """
    return 'bin:{}'.format(bitLength)

def packDataValue(data: ConstBitStream, values: dict, items: dict) -> dict:
    bitLength  = 1
    prevValue  = None
    prevNumber = None
    while True:
        format    = getEncoderKeyFormat(bitLength)
        newValue  = data.peek(format)
        lastBit   = data[data.bitpos+bitLength-1]
        if (bitLength + data.bitpos > len(data)):
            break
        if (newValue not in values):
            if (bitLength > 1):
                prevValueFormat = getEncoderKeyFormat(bitLength-1)
                prevValue       = data.peek(prevValueFormat)
                if (prevValue in values):
                    prevNumber = items[prevValue]
            break
        bitLength += 1
    return {
        'prevValue'  : prevValue,
        'prevNumber' : prevNumber,
        'newValue'   : newValue,
        'lastBit'    : lastBit,
        'bitLength'  : bitLength
    }

def getHashItemHeaderFormat(positionLengthBits: Optional[int]=ITEM_HEADER_POSITION_BITS, valueLengthBits: Optional[int]=ITEM_HEADER_VALUE_BITS) -> str:
    """
    >>> getHashItemHeaderFormat(2, 6)
    'bin:2,bin:6'
    >>> getHashItemHeaderFormat(4, 4)
    'bin:4,bin:4'
    """
    return 'bin:{},bin:{}'.format(positionLengthBits, valueLengthBits)

def getItemPositionLengthBits(extraPositionLength: int) -> int:
    return extraPositionLength + MIN_POSITION_LENGTH_BITS

def getItemPositionLengthBytes(extraPositionLength: int) -> int:
    return extraPositionLength + MIN_POSITION_LENGTH_BYTES

def getExtraPositionLengthBits(absolutePositionLength: int) -> int:
    return absolutePositionLength - MIN_POSITION_LENGTH_BITS

def getExtraPositionLengthBytes(absolutePositionLength: int) -> int:
    return absolutePositionLength - MIN_POSITION_LENGTH_BYTES

def getMinValueLengthBits(extraPositionLength: int, minPositionLength: Optional[int]=MIN_POSITION_LENGTH_BITS, headerLength: Optional[int]=ITEM_HEADER_LENGTH_BITS) -> int:
    return headerLength + extraPositionLength + minPositionLength

def getMinValueLengthBytes(positionLength: int, minScore: Optional[int]=0, headerLength: Optional[int]=ITEM_HEADER_LENGTH_BYTES) -> int:
    return headerLength + positionLength + minScore

def getMaxValueLengthBits(extraPositionLength: int, minPositionLength: Optional[int]=MIN_POSITION_LENGTH_BITS, headerLength: Optional[int]=ITEM_HEADER_LENGTH_BYTES) -> int:
    minLength = getMinValueLengthBits(extraPositionLength, minPositionLength, headerLength)
    return minLength + ((2 ** ITEM_HEADER_VALUE_BITS) - 1)

def getMaxValueLengthBytes(positionLength: int, minScore: Optional[int]=0, headerLength: Optional[int]=ITEM_HEADER_LENGTH_BYTES) -> int:
    minLength = getMinValueLengthBytes(positionLength, minScore, headerLength)
    return minLength + ((2 ** ITEM_HEADER_VALUE_BITS) - 1)

def getMinPositionLengthBytes() -> int:
    return MIN_POSITION_LENGTH_BYTES

def getMaxPositionLengthBytes() -> int:
    return MIN_POSITION_LENGTH_BYTES + ((2 ** ITEM_HEADER_POSITION_BITS) - 1)

def getItemValueLengthBytes(extraValueLength: int, extraPositionLength: int, headerLength: Optional[int]=ITEM_HEADER_LENGTH_BYTES) -> int:
    positionLength = getItemPositionLengthBytes(extraPositionLength)
    return headerLength + positionLength + extraValueLength

def getExtraValueLengthBits(valueLength: int, extraPositionLength: int, headerLength: Optional[int]=ITEM_HEADER_LENGTH_BITS) -> int:
    return valueLength - (extraPositionLength + headerLength)

def getExtraValueLengthBytes(valueLength: int, positionLength: int, headerLength: Optional[int]=ITEM_HEADER_LENGTH_BYTES) -> int:
    extraValueBitLength = getExtraValueLengthBits(valueLength, positionLength, headerLength * 8)
    return math.ceil((extraValueBitLength) / 8)

def getRelativePosition(absolutePosition: int, extraPositionLength: int) -> int:
    """
    >>> getRelativePosition(0, 0)
    0
    >>> getRelativePosition(255, 0)
    255
    >>> getRelativePosition(255, 8)
    127
    >>> getRelativePosition(256, 8)
    128
    >>> getRelativePosition(257, 8)
    129
    >>> getRelativePosition(256, 9)
    0
    """
    positionOffset = 0
    if (extraPositionLength > 0):
        positionOffset = (2 ** (extraPositionLength - 1))
    if (positionOffset > absolutePosition):
        raise Exception("Incorrect absolute position ({}) for extra position length={}".format(absolutePosition, extraPositionLength))
    return absolutePosition - positionOffset

def getAbsolutePosition(relativePosition: int, extraPositionLength: int) -> int:
    """
    >>> getAbsolutePosition(0, 0)
    0
    >>> getAbsolutePosition(255, 0)
    255
    >>> getAbsolutePosition(0, 8)
    256
    >>> getAbsolutePosition(0, 16)
    65536
    """
    positionOffset = 0
    if (extraPositionLength > 0):
        positionOffset = (2 ** extraPositionLength)
    return relativePosition + positionOffset

def createHeaderFromData(value: Bits, absolutePosition: int) -> Bits:
    """
    """
    valueLength            = len(value)
    absolutePositionLength = bitLen(absolutePosition)
    relativePositionLength = getExtraPositionLengthBits(absolutePositionLength)
    minValueLength         = getMinValueLengthBits(relativePositionLength)
    maxValueLength         = getMaxValueLengthBits(relativePositionLength)
    if (valueLength < minValueLength):
        raise Exception("Value is too short (got {} bits, min {} bits): cannot create item header".format(valueLength, minValueLength))
    if (valueLength > maxValueLength):
        raise Exception("Value is too long (got {} bits, max {} bits): cannot create item header".format(valueLength, maxValueLength))
    if (relativePositionLength > MAX_POSITION_LENGTH_BITS):
        raise Exception("Position is too long (got {} bits, max {} bits): cannot create item header".format(absolutePositionLength, MAX_POSITION_LENGTH_BITS))
    valueHeader    = Bits(uint=getExtraValueLengthBits(valueLength, absolutePositionLength), length=ITEM_HEADER_VALUE_BITS)
    positionHeader = Bits(uint=relativePositionLength, length=ITEM_HEADER_VALUE_BITS)
    return (positionHeader + valueHeader)

def createItemHeader(value: Bits, position: int) -> Bits:
    """
    >>> createItemHeader(Bits(uint=32767, length=16), 255)
    Bits('0x00')
    >>> createItemHeader(Bits(uint=32767, length=17), 255)
    Bits('0x01')
    >>> createItemHeader(Bits(uint=32767, length=23), 2**15-1)
    Bits('0x70')
    >>> createItemHeader(Bits(uint=32767, length=23), 2**15-1).bin
    '01110000'
    >>> createItemHeader(Bits(uint=32767, length=32), 2**23-1).bin
    '11110001'
    >>> createItemHeader(Bits(uint=32767, length=31), 2**23-1).bin
    '11110000'
    """
    return createHeaderFromData(value, position)

def createItemData(value: Bits, absolutePosition: int) -> Bits:
    """
    >>> createItemData(Bits(uint=0, length=16), 128).bin
    '00000000'
    >>> #createItemData(Bits(uint=0, length=17), 129).bin
    '00000001'
    >>> #createItemData(Bits(uint=0, length=16), 255).bin
    '01111111'
    >>> #createItemData(Bits(uint=0, length=16), 256).bin
    '000000000'
    """
    absolutePositionLength = bitLen(absolutePosition)
    positionDataLength     = absolutePositionLength
    if (absolutePositionLength <= MIN_POSITION_LENGTH_BITS):
        positionDataLength = MIN_POSITION_LENGTH_BITS
    extraPositionLength = getExtraPositionLengthBits(absolutePositionLength)
    position            = getRelativePosition(absolutePosition, absolutePositionLength)
    data = Bits(uint=position, length=positionDataLength)
    extraValueLength = getExtraValueLengthBits(len(value), absolutePositionLength)
    if (extraValueLength > 0):
        data = data + Bits(uint=extraValueLength, length=extraValueLength)
    return data

