from hashlib import sha256
from collections import OrderedDict
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, ConstBitArray, ConstBitStream

# prefix bits used to define position number
PREFIX_POSITION_LENGTH = 5
# prefix bits used to define length of value that we read from position
# VALUE_LENGTH = BASE_SCORE + POSITION_LENGTH + VALUE_SCORE
PREFIX_SCORE_LENGTH    = 3
# bit length of the prefix before value (default is 8)
PREFIX_LENGTH          = PREFIX_POSITION_LENGTH + PREFIX_SCORE_LENGTH
# minimal score for values stored in base
BASE_SCORE             = PREFIX_LENGTH
# number of bits used as base step when searching values (8 for byte-aligned search)
POSITION_STEP          = 4
# hash digest size (bits)
HASH_LENGTH            = 256
# maximum absolute position
MAX_PREFIX_POSITION = (2 ** PREFIX_POSITION_LENGTH) - 1
# maximum score for value
MAX_PREFIX_SCORE    = (2 ** PREFIX_SCORE_LENGTH) - 1

def toAbsoluteBitPosition(position):
    return position * POSITION_STEP

def fromAbsoluteBitPosition(position):
    return position // POSITION_STEP

def toAbsoluteBitLength(value):
    return len(value) * POSITION_STEP

def fromAbsoluteBitLength(value):
    return len(value) // POSITION_STEP

def toPositionLength(prefix):
    """
    >>> toPositionLength(255)
    31
    """
    prefix = Bits(uint=prefix, length=PREFIX_LENGTH)
    return prefix[0:PREFIX_POSITION_LENGTH].uint

def toScoreLength(prefix):
    """
    >>> toScoreLength(255)
    7
    """
    start  = PREFIX_LENGTH - PREFIX_SCORE_LENGTH
    prefix = Bits(uint=prefix, length=PREFIX_LENGTH)
    return prefix[start:PREFIX_LENGTH].uint

def toValueLength(prefix):
    prefix         = Bits(uint=prefix, length=PREFIX_LENGTH)
    positionLength = toPositionLength(prefix.uint)
    scoreLength    = toScoreLength(prefix.uint)
    valueLength    = PREFIX_LENGTH + positionLength + scoreLength
    return valueLength

def toMinValueScore(prefix):
    prefix         = Bits(uint=prefix, length=PREFIX_LENGTH)
    positionLength = toPositionLength(prefix.uint)
    minScore       = PREFIX_LENGTH + positionLength
    return minScore

def toAbsoluteHashPosition(bitPosition):
    return bitPosition // HASH_LENGTH

def createPrefixes():
    """
    >>> prefixes = createPrefixes()
    >>> len(prefixes)
    256
    >>> prefixes[255].hex
    'ff'
    >>> prefixes[255].uint
    255
    >>> toPositionLength(prefixes[255].uint)
    31
    >>> toScoreLength(prefixes[255].uint)
    7
    >>> toPositionLength(prefixes[0].uint)
    0
    >>> toScoreLength(prefixes[0].uint)
    0
    """
    prefixes = []
    for positionLength in range(0, 2 ** PREFIX_POSITION_LENGTH):
        posPrefix   = BitArray(uint=positionLength, length=PREFIX_POSITION_LENGTH)
        for scoreLength in range(0, 2 ** PREFIX_SCORE_LENGTH):
            scorePrefix = BitArray(uint=scoreLength, length=PREFIX_SCORE_LENGTH)
            prefix = posPrefix + scorePrefix
            prefixes.append(prefix)
    return prefixes

def createHexRange(startRange, endRange):
    """ 
    >>> createHexRange(0, 1)
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> len(createHexRange(0, 1))
    64
    >>> createHexRange(0, 2)
    'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b8556e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    >>> len(createHexRange(0, 2))
    128
    >>> createHexRange(1, 2)
    '6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d'
    >>> createHexRange(1, 3)
    '6e340b9cffb37a989ca544e6bb780a2c78901d3fb33738768511a30617afa01d96a296d224f285c67bee93c30f8a309157f0daa35dc5b87e410b78630a09cfc7'
    """
    result = ''
    for i in range(startRange, endRange):
        result += sha256(bytes(i)).hexdigest()
    return result

def findInHex(hexRange, value, start=0, end=None):
    """
    >>> findInHex('e3b0c44298fc', 'c')
    4
    >>> findInHex('e3b0c44298fc', 'c', 0, 3)
    -1
    >>> findInHex('e3b0c44298fc', '0c')
    3
    >>> findInHex('e3b0c44298fc', '4')
    5
    >>> findInHex('e3b0c44298fc', '4', 6)
    6
    >>> findInHex('e3b0c44298fc', '4', 7)
    -1
    """
    if (end == None):
        end = len(hexRange)
    return hexRange.find(value, start, end)

def findMinUniquePrefix(hexRange, uniqueValues, minLength=1):
    """
    >>> findMinUniquePrefix('e3b0c44298fc', {})
    'e'
    >>> findMinUniquePrefix('e3b0c44298fc', {'e': 0})
    'e3'
    >>> findMinUniquePrefix('e3b0c44298fc', {'e': 0}, 4)
    'e3b0'
    >>> findMinUniquePrefix('e3b0c44298fc', {'e': 0, 'e3': 0})
    'e3b'
    >>> findMinUniquePrefix('e3b0c44298fc', {'e3': 0})
    'e'
    >>> findMinUniquePrefix('e3b', {'e': 0, 'e3': 0, 'e3b': 0})
    """
    for position in range(0, len(hexRange)):
        start = 0
        end   = minLength + position
        value = hexRange[start:end]
        if (value not in uniqueValues):
            return value
    return None

def findMaxCommonPrefix(hexRange, uniqueValues, minLength=1, maxLength=None):
    """
    >>> findMaxCommonPrefix('e3b0c44298fc', {})
    >>> findMaxCommonPrefix('e3b0c44298fc', {'e': 0})
    'e'
    >>> findMaxCommonPrefix('e3b0c44298fc', {'e': 0}, 4)
    >>> findMaxCommonPrefix('e3b0c44298fc', {'e': 0, 'e3': 0})
    'e3'
    >>> findMaxCommonPrefix('e3b0c44298fc', {'e3': 0})
    'e3'
    """
    maxValue = None
    for position in range(0, len(hexRange)):
        start = 0
        end   = minLength + position
        value = hexRange[start:end]
        if (value in uniqueValues):
            maxValue = value
    return maxValue

def findUniqueValues(hexRange, uniqueValues, length=1, startRange=0, endRange=None):
    """
    Find all unique values with same length
    >>> findUniqueValues('e3b0c44298fc', {'e': 0, '3': 1, 'b': 2, '0': 3, 'c': 4, '4': 5, '2': 7, '9': 8, '8': 9, 'f': 10})
    {}
    >>> findUniqueValues('e3b0c44298fc', {})
    {'e': 0, '3': 1, 'b': 2, '0': 3, 'c': 4, '4': 5, '2': 7, '9': 8, '8': 9, 'f': 10}
    >>> findUniqueValues('e3b0c44298fc', {}, 1, 8)
    {'9': 8, '8': 9, 'f': 10}
    >>> findUniqueValues('e3b0c44298fc', {}, 1, 8, 9)
    {'9': 8, '8': 9}
    >>> findUniqueValues('e3b0c44298fc', {'e': 0, '3': 1, 'b': 2})
    {'0': 3, 'c': 4, '4': 5, '2': 7, '9': 8, '8': 9, 'f': 10}
    >>> findUniqueValues('e3b0c44298fc', {}, 2)
    {'e3': 0, '3b': 1, 'b0': 2, '0c': 3, 'c4': 4, '44': 5, '42': 6, '29': 7, '98': 8, '8f': 9}
    >>> findUniqueValues('e3b0e3b0e3b0e3b0', {}, 2)
    {'e3': 0, '3b': 1, 'b0': 2, '0e': 3}
    >>> findUniqueValues('e3b0e3b0e3b0e3b0', {'e3': 0, '3b': 1, 'b0': 2, '0e': 3}, 2)
    {}
    >>> allUniqueValues = findUniqueValues('1234567890abcdef1234567890abcdef', {})
    >>> allUniqueValues
    {'1': 0, '2': 1, '3': 2, '4': 3, '5': 4, '6': 5, '7': 6, '8': 7, '9': 8, '0': 9, 'a': 10, 'b': 11, 'c': 12, 'd': 13, 'e': 14, 'f': 15}
    >>> len(allUniqueValues)
    16
    """
    newUniqueValues = {}
    allUniqueValues = uniqueValues.copy()
    if (endRange == None):
        endRange = len(hexRange) - length
    else:
        endRange = endRange + length
    if (endRange > len(hexRange)):
        raise Exception("Incorrect endRange")
    if (length <= 0):
        raise Exception("Incorrect length")
    # number of maximum unique values fixed length available for hex encoding
    maxUniqueValues = 2 ** (length * 4)
    for position in range(startRange, endRange):
        start = position
        end   = start + length
        value = hexRange[start:end]
        if (value not in allUniqueValues):
            newUniqueValues.update({value: start})
            allUniqueValues.update({value: start})
            # no way to get more unique values that possible
            if (len(allUniqueValues) == maxUniqueValues):
                break
    return newUniqueValues

def extractMissingDictValues(hexRange, uniqueValues, length, startRange=0, endRange=None):
    """
    Find all remaining values for the unique value dictionary of fixed length
    >>> hexRange = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> extractMissingDictValues(hexRange, {'4': 5, '2': 7, '9': 8, '8': 9, 'f': 10, '1': 12, 'a': 17, '6': 26, '7': 33, '5': 51}, 1)
    {'e': 0, '3': 1, 'b': 2, '0': 3, 'c': 4}
    >>> extractMissingDictValues(hexRange, {}, 1, 1, 4)
    {'3': 1, 'b': 2, '0': 3, 'c': 4}
    """
    extractedValues = findUniqueValues(hexRange, uniqueValues, length, startRange, endRange)
    return extractedValues

def extractHexValueDict(hexRange, minLength=1, maxLength=1, startRange=0, endRange=None):
    """
    >>> hexRange = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'
    >>> extractHexValueDict(hexRange)
    {1: {'e': 0, '3': 1, 'b': 2, '0': 3, 'c': 4, '4': 5, '2': 7, '9': 8, '8': 9, 'f': 10, '1': 12, 'a': 17, '6': 26, '7': 33, '5': 51}}
    >>> extractHexValueDict(hexRange, 2, 2)
    {2: {'e3': 0, '3b': 1, 'b0': 2, '0c': 3, 'c4': 4, '44': 5, '42': 6, '29': 7, '98': 8, '8f': 9, 'fc': 10, 'c1': 11, '1c': 12, '14': 14, '49': 15, '9a': 16, 'af': 17, 'fb': 18, 'bf': 19, 'f4': 20, '4c': 21, 'c8': 22, '89': 23, '99': 24, '96': 25, '6f': 26, 'b9': 28, '92': 29, '24': 30, '27': 32, '7a': 33, 'ae': 34, 'e4': 35, '41': 36, '1e': 37, '46': 39, '64': 40, '9b': 42, '93': 44, '34': 45, 'ca': 47, 'a4': 48, '95': 50, '59': 51, '91': 53, '1b': 54, 'b7': 55, '78': 56, '85': 57, '52': 58, '2b': 59, 'b8': 60}}
    """
    lengthDicts = {}
    for length in range(minLength, maxLength + 1):
        lengthDict = findUniqueValues(hexRange, {}, length, startRange, endRange)
        lengthDicts.update({length : lengthDict})
    return lengthDicts

def mapHexRange(hexRange, mapping={}, minLength=1):
    """
    >>> mapHexRange('e3b0c44298fc', {})
    {'mapping': OrderedDict([('e', 0), ('3', 1), ('b', 2), ('0', 3), ('c', 4), ('4', 5), ('42', 6), ('9', 8), ('8', 9), ('f', 10)]), 'tail': 'c'}
    >>> mapHexRange('e3b0c44298fc', {}, 2)
    {'mapping': OrderedDict([('e3', 0), ('b0', 2), ('c4', 4), ('42', 6), ('98', 8), ('fc', 10)]), 'tail': ''}
    """
    mapping  = OrderedDict(mapping)
    position = 0
    value    = findMinUniquePrefix(hexRange, mapping, minLength)
    while (value != None):
        mapping.update({value: position})
        length   = len(value)
        hexRange = hexRange[length:len(hexRange)]
        value    = findMinUniquePrefix(hexRange, mapping, minLength)
        position = position + length
    return {
        'mapping' : mapping,
        'tail'    : hexRange
    }
    
def mapHexData(hexData, mapping, minLength=1):
    """
    >>> hexData  = 'f5a5fd42d16a2'
    >>> hexRange = createHexRange(0, 4096)
    >>> hexMap   = mapHexRange(hexRange)
    >>> mapHexData(hexData, hexMap['mapping'], 3)
    [{'id': 0, 'v': 'f5a5', 'p': 4096, 'l': 4, 's': 0, 'e': 4}, {'id': 1, 'v': 'fd4', 'p': 4100, 'l': 3, 's': 4, 'e': 7}, {'id': 2, 'v': '2d16', 'p': 162478, 'l': 4, 's': 7, 'e': 11}, {'id': 3, 'v': 'a2', 'p': 130, 'l': 2, 's': 11, 'e': 13}]
    """
    result     = []
    partNumber = 0
    fullLength = len(hexData)
    start      = 0
    while (len(hexData) > 0):
        part = findMaxCommonPrefix(hexData, mapping, minLength)
        if (part == None):
            raise Exception("Mapping dictionary is inconsistent for provided data")
        start    = fullLength - len(hexData)
        end      = start + len(part)
        position = mapping[part]
        result.append({'id': partNumber, 'v': part, 'p': position, 'l': len(part), 's': start, 'e': end})
        # remove mapped part
        hexData    = hexData[len(part):len(hexData)]
        partNumber = partNumber + 1
    return result