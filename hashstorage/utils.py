# https://docs.python.org/3/library/typing.html
from typing import Any, List, Dict, Optional, Type, TypeVar, Text, Generic, Literal, Union
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pyrlp.readthedocs.io/en/latest/index.html
import rlp
# https://pyrlp.readthedocs.io/en/latest/tutorial.html
from rlp.sedes import (
    boolean,
    binary,
    big_endian_int,
    text,
    List
)
# https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html
import pandas as pd
import numpy as np
from hashlib import sha256

HASH_SIZE_BITS  = 256
HASH_SIZE_BYTES = HASH_SIZE_BITS // 8

def bitLen(value: int) -> int: 
    """
    >>> bitLen(0)
    1
    >>> bitLen(1)
    1
    >>> bitLen(7)
    3
    >>> bitLen(8)
    4
    """
    if value < 0:
        raise Exception("Only unsigned integers allowed")
    # 1 bit is an absolute minimum
    if value == 0:
        return 1
    length = 0
    while (value):
        value >>= 1
        length += 1
    return length

def positionToNounce(bytePosition: int) -> int:
    """
    >>> positionToNounce(0)
    0
    >>> positionToNounce(1)
    0
    >>> positionToNounce(31)
    0
    >>> positionToNounce(32)
    1
    >>> positionToNounce(63)
    1
    >>> positionToNounce(64)
    2
    """
    return (bytePosition // HASH_SIZE_BYTES)

def toLocalPosition(bytePosition: int) -> int:
    """
    >>> toLocalPosition(0)
    0
    >>> toLocalPosition(1)
    1
    >>> toLocalPosition(31)
    31
    >>> toLocalPosition(32)
    0
    >>> toLocalPosition(33)
    1
    >>> toLocalPosition(63)
    31
    >>> toLocalPosition(64)
    0
    """
    nounce = positionToNounce(bytePosition)
    offset = HASH_SIZE_BYTES * nounce
    return (bytePosition - offset)

def nounceToHash(nounce: int) -> bytes:
    """ Normalized hash generation
    >>> nounceToHash(0).hex()
    '76be8b528d0075f7aae98d6fa57a6d3c83ae480a8469e668d7b0af968995ac71'
    >>> nounceToHash(1).hex()
    '4bf5122f344554c53bde2ebb8cd2b7e3d1600ad631c385a5d7cce23c7785459a'
    """
    input = rlp.encode(nounce, big_endian_int)
    return sha256(input).digest()

def createHashRange(startNounce: int, endNounce: int) -> Bits:
    """ 
    >>> createHashRange(0, 2)
    BitArray('0x76be8b528d0075f7aae98d6fa57a6d3c83ae480a8469e668d7b0af968995ac714bf5122f344554c53bde2ebb8cd2b7e3d1600ad631c385a5d7cce23c7785459a')
    """
    hashBits = BitArray()
    for nounce in range(startNounce, endNounce):
        hashBytes = nounceToHash(nounce)
        hashBits.append(hashBytes)
    return hashBits

def encodeRlpValue(value: int) -> Bits:
    """
    >>> encodeRlpValue(129)
    Bits('0x8181')
    >>> encodeRlpValue(0)
    Bits('0x80')
    """
    return Bits(rlp.encode(value, big_endian_int))

def decodeRlpValue(value: Bits) -> int:
    """
    >>> decodeRlpValue(Bits('0x8181'))
    129
    >>> decodeRlpValue(Bits('0x80'))
    0
    """
    return rlp.decode(value.bytes, big_endian_int)

def getDecodedRlpLength(value: Bits) -> int:
    """
    >>> getDecodedRlpLength(Bits('0x8181'))
    1
    >>> getDecodedRlpLength(Bits('0x80'))
    1
    >>> encodedValue = encodeRlpValue(65535)
    >>> len(encodedValue.bytes)
    3
    >>> getDecodedRlpLength(encodedValue)
    2
    """
    value      = decodeRlpValue(value)
    byteLength = bitLen(value) // 8
    if (byteLength == 0):
        byteLength = 1
    return byteLength

def createHashRangeForValue(value: Bits, start: Optional[int]=None, end: Optional[int]=None) -> Bits:
    """
    >>> hashRange = createHashRangeForValue(Bits('0x07'))
    >>> len(hashRange) // 8
    256
    >>> hashRange = createHashRangeForValue(Bits('0x8182'))
    >>> len(hashRange) // 8
    65536
    """
    bitLength  = len(value)
    byteLength = bitLength // 8
    minByte    = 0
    maxByte    = 2 ** (byteLength * 8)
    if (start == None):
        start = minByte
    if (end == None):
        end = maxByte
    startNounce = positionToNounce(start)
    endNounce   = positionToNounce(end)
    return createHashRange(startNounce, endNounce)

def sliceHashRange(hashRange: BitArray, byteLength: int, start: Optional[int]=None, end: Optional[int]=None) -> List:
    """
    >>> hashRange = createHashRange(0, 1)
    >>> sliceHashRange(hashRange, 8)
    [Bits('0x76be8b528d0075f7'), Bits('0xaae98d6fa57a6d3c'), Bits('0x83ae480a8469e668'), Bits('0xd7b0af968995ac71')]
    """
    items     = []
    bitLength = byteLength * 8
    for item in hashRange.cut(bitLength):
        items.append(Bits(item))
    return items

def getHashBytes(hashRange: BitArray, bytePosition: int, length: int) -> Bits:
    """
    >>> hashRange = createHashRange(0, 1)
    >>> getHashBytes(hashRange, 19, 1)
    Bits('0x0a')
    """
    start = bytePosition * 8
    end   = start + (length * 8)
    return Bits(hashRange[start:end])

def createRlpValues(byteLength: int, start: Optional[int]=None, end: Optional[int]=None) -> Dict:
    """
    >>> createRlpValues(1, 0, 8)
    {Bits('0x80'): 0, Bits('0x01'): 1, Bits('0x02'): 2, Bits('0x03'): 3, Bits('0x04'): 4, Bits('0x05'): 5, Bits('0x06'): 6, Bits('0x07'): 7}
    >>> len(createRlpValues(1, 0, 8))
    8
    >>> len(createRlpValues(1))
    256
    >>> createRlpValues(1, 125, 131)
    {Bits('0x7d'): 125, Bits('0x7e'): 126, Bits('0x7f'): 127, Bits('0x8180'): 128, Bits('0x8181'): 129, Bits('0x8182'): 130}
    """
    values    = dict()
    bitLength = byteLength * 8
    minValue  = 0
    maxValue  = 2 ** bitLength
    if (start == None):
        start = minValue
    if (end == None):
        end = maxValue
    for intValue in range(start, end):
        value = rlp.encode(intValue, big_endian_int)
        values.update({Bits(bytes=value): intValue})
    return values

def findValuePosition(value: Bits, hashRange: Optional[BitArray]=None, start: Optional[int]=None, end: Optional[int]=None) -> int:
    """
    >>> hashRange = createHashRange(0, 1)
    >>> findValuePosition(Bits('0xff'), hashRange)
    >>> findValuePosition(Bits('0xff'))
    69
    >>> findValuePosition(Bits('0x0a'), hashRange)
    19
    >>> findValuePosition(Bits('0x0a'))
    19
    >>> getHashBytes(hashRange, findValuePosition(Bits('0x0a'), hashRange), 1)
    Bits('0x0a')
    """
    if (hashRange == None):
        hashRange = createHashRangeForValue(value, start, end)
    # https://bitstring.readthedocs.io/en/latest/constbitarray.html#bitstring.Bits.find
    bitPosition = hashRange.find(value, bytealigned=True)
    if (len(bitPosition) < 1):
        return None
    return (bitPosition[0] // 8)

def findRlpValues(values: Dict, start: Optional[int]=None, end: Optional[int]=None) -> Dict:
    """
    >>> values = createRlpValues(1, 0, 4)
    >>> values
    {Bits('0x80'): 0, Bits('0x01'): 1, Bits('0x02'): 2, Bits('0x03'): 3}
    >>> findRlpValues(values)
    {Bits('0x80'): None, Bits('0x01'): 79, Bits('0x02'): None, Bits('0x03'): None}
    >>> values = createRlpValues(1, 126, 130)
    >>> values
    {Bits('0x7e'): 126, Bits('0x7f'): 127, Bits('0x8180'): 128, Bits('0x8181'): 129}
    >>> findRlpValues(values)
    {Bits('0x7e'): 230, Bits('0x7f'): None, Bits('0x8180'): None, Bits('0x8181'): None}
    """
    result          = dict()
    hashRange       = None
    prevValueLength = 0
    for value in values.keys():
        # rlp-encoded values can consume more bytes than original data - that's why we need to know it's original size
        byteLength = getDecodedRlpLength(value)
        if (prevValueLength != byteLength):
            hashRange = createHashRangeForValue(value)
        position = findValuePosition(value, hashRange, start, end)
        result.update({value: position})
        prevValueLength = byteLength
    return result

def filterMissingValues(values: Dict):
    """
    >>> values = createRlpValues(1, 126, 130)
    >>> values
    {Bits('0x7e'): 126, Bits('0x7f'): 127, Bits('0x8180'): 128, Bits('0x8181'): 129}
    >>> rlpValues = findRlpValues(values)
    >>> rlpValues
    {Bits('0x7e'): 230, Bits('0x7f'): None, Bits('0x8180'): None, Bits('0x8181'): None}
    >>> filterMissingValues(rlpValues)
    {Bits('0x7f'): None, Bits('0x8180'): None, Bits('0x8181'): None}
    """
    missingValues = dict()
    for value in values.keys():
        if (values[value] == None):
            missingValues.update({value: values[value]})
    return missingValues

def extendMissingValue(value: Bits) -> Dict:
    """
    >>> extensions = extendMissingValue(Bits('0x80'))
    >>> extKeys    = list(extensions.keys())
    >>> len(extensions)
    256
    >>> extensions[extKeys[0]]
    0
    >>> extKeys[0]
    Bits('0xc28080')
    >>> extensions[extKeys[1]]
    1
    >>> extKeys[1]
    Bits('0xc28001')
    >>> extKeys[255]
    Bits('0xc38081ff')
    >>> extensions[extKeys[255]]
    255
    """
    baseValue     = decodeRlpValue(value)
    extensions    = dict()
    for extensionValue in range(0, 256):
        # extend missing value with all possible options - if we can found all of them we coud use those values instead of more short missing value
        extension = rlp.encode([baseValue, extensionValue])
        # extension = rlp.encode(extension, big_endian_int)
        # extension = rlp.encode(extensionValue)
        extensions.update({Bits(bytes=extension):extensionValue})
    return extensions

def createFormat(positionBits, valueBits):
    """
    >>> createFormat(2, 6)
    'uint:2,uint:6'
    >>> createFormat(0, 0)
    ''
    >>> createFormat(1, 0)
    'uint:1'
    >>> createFormat(0, 2)
    'uint:2'
    """
    format = ''
    if (positionBits > 0):
        format += "uint:{0}".format(positionBits)
    if (valueBits > 0):
        if (format != ''):
            format += ','
        format += "uint:{0}".format(valueBits)
    return format