# https://docs.python.org/3/library/typing.html
from typing import Any, List, Dict, Optional, Type, TypeVar, Text, Generic, Literal, Union
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
from hashlib import sha256

# https://stackoverflow.com/questions/30081275/why-is-1000000000000000-in-range1000000000000001-so-fast-in-python-3?rq=1
# https://towardsdatascience.com/custom-iteration-patterns-with-python-generators-12b98e552b38

# scanner is a custom iterator that produces data on the fly based on input
# same input always produces same result


def scan_pattern(start, stop, increment):
    x = start
    while x <= stop:
        yield x
        x += increment
