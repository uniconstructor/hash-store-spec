# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, ConstBitStream
from .utils import bitLen, positionToNounce, toLocalPosition

ITEM_HEADER_PREFIX_BITS   = 4
ITEM_HEADER_PREFIX_OFFSET = 2
ITEM_HEADER_ITEM_MIN_BITS = 2
RESERVED_HEADER_VALUE     = Bits(uint=0, length=8)

def getHeaderPrefix(header: Bits) -> Bits:
    return header[0:ITEM_HEADER_PREFIX_BITS]

def getHeaderItemLength(header: Bits) -> int:
    prefix = getHeaderPrefix(header)
    return (prefix.uint + ITEM_HEADER_PREFIX_OFFSET)

def getHeaderDataLength(header: Bits) -> int:
    itemLength = getHeaderItemLength(header)
    return (itemLength * 2)

def getHeaderData(header: Bits) -> Bits:
    dataStart  = ITEM_HEADER_PREFIX_BITS
    dataLength = getHeaderDataLength(header)
    dataEnd    = dataStart + dataLength
    return header[dataStart:dataEnd]

def getHeaderLength(header: Bits) -> int:
    """
    >>> getHeaderLength(Bits('0b00000000'))
    8
    >>> getHeaderLength(Bits('0b0001000000'))
    10
    """
    headerDataLength = getHeaderDataLength(header)
    headerLength     = ITEM_HEADER_PREFIX_BITS + headerDataLength
    return headerLength

def getPositionItem(header: Bits) -> Bits:
    """
    >>> getPositionItem(Bits('0b00000000'))
    Bits('0b00')
    >>> getPositionItem(Bits('0b0001001000'))
    Bits('0b001')
    >>> getPositionItem(Bits('0b001000001111')).bin
    '0000'
    """
    headerData     = getHeaderData(header)
    positionLength = getHeaderItemLength(header)
    start          = 0
    end            = start + positionLength
    return headerData[start:end]

def getValueItem(header: Bits) -> Bits:
    """
    >>> getValueItem(Bits('0b00000000'))
    Bits('0b00')
    >>> getValueItem(Bits('0b0001000001'))
    Bits('0b001')
    >>> getValueItem(Bits('0b001000001111')).bin
    '1111'
    """
    headerData     = getHeaderData(header)
    positionLength = getHeaderItemLength(header)
    valueLength    = getHeaderItemLength(header)
    start          = positionLength
    end            = start + valueLength
    return headerData[start:end]

def getPrevCheckpointLength(currentLength: int) -> int:
    if (currentLength < 2):
        return 1
    return currentLength - 1

def getNextCheckpointLength(currentLength: int) -> int:
    return currentLength + 1

def getCheckpointCapacityByLength(currentLength: int) -> int:
    """
    """
    if (currentLength < 1):
        raise Exception('Incorrect length provded for checkpoint range calculation: {}'.format(currentLength))
    return 2 ** currentLength

def getDistanceFromPrevCheckpoint(relativePosition: Bits):
    """
    >>> getDistanceFromPrevCheckpoint(Bits('0b00'))
    0
    >>> getDistanceFromPrevCheckpoint(Bits('0b01'))
    1
    >>> getDistanceFromPrevCheckpoint(Bits('0b000'))
    0
    >>> getDistanceFromPrevCheckpoint(Bits('0b001'))
    1
    >>> getDistanceFromPrevCheckpoint(Bits('0b010'))
    2
    """
    return relativePosition.uint

def getCheckpointByNumber(number: int) -> int:
    """ Generates checkpoint value (as Bits() object) using it's number
    >>> getCheckpointByNumber(0)
    Bits('0b0')
    >>> getCheckpointByNumbfancyer(1)
    Bits('0b00')
    >>> getCheckpointByNumber(2)
    Bits('0b000')
    """
    if (number == 0):
        return Bits('0b0')
    return (Bits('0b0') * number)

def getCheckpointLengthByNumber(number: int) -> int:
    """ Calculates length of the checkpoint value (in bits) using it's number
    Checkpoints have their own enumeration, that includes only this values and used as another layer,
    separated from global relative positions values enumeration
    >>> getCheckpointLengthByNumber(0)
    1
    >>> getCheckpointLengthByNumber(1)
    2
    >>> getCheckpointLengthByNumber(2)
    3
    """
    return len(getCheckpointByNumber(number))

def getCheckpointNumberByLength(checkpointLength: int) -> int:
    """
    >>> getCheckpointNumberByLength(1)
    0
    >>> getCheckpointNumberByLength(2)
    1
    >>> getCheckpointNumberByLength(3)
    2
    """
    if (checkpointLength < 1):
        raise Exception('Checkpoint number not found: incorrect checkpoint length provided ({})'.format(checkpointLength))
    return checkpointLength - 1

def getTotalCapacityBeforeLength(checkpointLength: int) -> int:
    """ total capacity of all previous checkpoints below given length - from first to last
    (last checkpoint is the longest and defined by length, it's capacity does not included in total sum)
    >>> getTotalCapacityBeforeLength(1)
    0
    >>> getTotalCapacityBeforeLength(2)
    2
    >>> getTotalCapacityBeforeLength(3)
    6
    >>> getTotalCapacityBeforeLength(4)
    14
    """
    totalCapacity    = 0
    checkpointNumber = getCheckpointNumberByLength(checkpointLength)
    # collecting capacity of all previous checkpoints
    for currentNumber in range(0, checkpointNumber):
        currentLength   = getCheckpointLengthByNumber(currentNumber)
        # total number of values inside this checkpoint, all sharing same length
        currentCapacity = getCheckpointCapacityByLength(currentLength)
        totalCapacity  += currentCapacity
    return totalCapacity

def getCheckpointPositionByNumber(number: int) -> int:
    """ Find absolute position of the checkpoint with given number
    >>> getCheckpointPositionByNumber(0)
    0
    >>> getCheckpointPositionByNumber(1)
    2
    >>> getCheckpointPositionByNumber(3)
    14
    """
    checkpointLength = getCheckpointLengthByNumber(number)
    # total capacity of all previous checkpoints - from first to last (defined by length)
    parentCapacity   = getTotalCapacityBeforeLength(checkpointLength)
    return parentCapacity

def getPrevCheckpointAbsolutePosition(relativePosition: Bits) -> int:
    """
    >>> getPrevCheckpointAbsolutePosition(Bits('0b01'))
    2
    """
    currentLength          = len(relativePosition)
    prevCheckpointLength   = currentLength
    prevCheckpointNumber   = getCheckpointNumberByLength(prevCheckpointLength)
    return getCheckpointPositionByNumber(prevCheckpointNumber)

def getNextPositionLength(currentNumber: int, currentLength: int) -> int:
    """
    """
    return 

def getRelativePositionLengthByNumber(positionNumber: int) -> Bits:
    """
    """
    return

def getRelativePositionByNumber(positionNumber: int) -> Bits:
    """ 
    """
    return

def getNextCheckpointByPrevValue(prevCheckpoint: Bits) -> int:
    """
    """
    return

def getNextCheckpointByPrevPosition(prevAbsolutePosition: int) -> int:
    """ 
    """
    return

def getPrevCheckpointPosition(absolutePosition: int) -> int:
    """
    """
    return

def getRelativePositionFromAbsolute(absolutePosition: int) -> Bits:
    """ Creates relative position value using absolute position (unsigned int)
    This function not aware about length of the **value** located at given position.
    It is only converts position value from one format to another
    """
    return

def getAbsolutePositionFromRelative(relativePosition: Bits) -> int:
    """ Find absolute value position using relative position of this value
    Relative position is a unsigned integer, representing distance back to last closest relative checkpoint value
    """
    return

def getRelativeOffset(absolutePosition: int) -> Bits:
    """ Helper function - calculates difference between two type of ordering: 
    1) standard unsigned int encoding (takes minimum number of bits, discard not used leading zeroes if any, start from 0)
    Examples:
    0: -> 0, 
    1: -> 1, 
    2: -> 10, 
    3: -> 11, 
    4: -> 100, 
    5: -> 101,
    ...

    2) custom length-based encoding (numeration starts from 0 every time when new length reached,
    leading zeroes are not discarded - number of bits used to represent value depends on previously used numbers)
    Length of the value increases only when we use up all bit combibations with current length
    This type of encoding requires an additional metadata: length of each value must be provided before sequence parsing 
    Examples: 
    0: -> 0,
    1: -> 1,
    2: -> 00,
    3: -> 01,
    4: -> 10,
    5: -> 11,
    6: -> 100,
    ...

    It converts absolute value position (unsigned int) to length-based position (unsigned int, represented by Bits object)
    Integer value does not allow us to store value length directly, but Bits() object does
    """
    currentPositionLength = bitLen(absolutePosition)
    if (currentPositionLength == 1):
        return 0
    # checkpoint is a position with number 2**n: when we need to add one bit to number length every time
    # when we passing one of this positions 
    # in other words - checkpoint is the position of every **new** and **first** occurance for the value 
    # with length that never used before
    prevCheckpointLength = currentPositionLength - 1
    nextCheckpointLength = prevCheckpointLength + 1
    # absolute positions of previous and next checkpoints
    prevCheckpointPosition = 2 ** prevCheckpointLength
    nextCheckpointPosition = 2 ** nextCheckpointLength
    # if current position exactly maches with any checkpoint we consider our position as 
    # **previous checkpoint** (with 0 distance to it) and next checkpoint will be set as next power of 2
    
    # distance from current position back to the last checkpoint position
    # length of the value equals with the minimal number of bits needed to represent absolute position value
    # enumeratation starts from 0, where 0 matches last checkpoint absolute position
    distanceFromLastCheckpoint = absolutePosition - prevCheckpointPosition
    return distanceFromLastCheckpoint

def createRelativePosition(absolutePosition: int) -> Bits:
    """
    >>> createRelativePosition(0).bin
    '0'
    >>> createRelativePosition(1).bin
    '1'
    >>> createRelativePosition(2).bin
    '00'
    >>> createRelativePosition(3).bin
    '01'
    >>> createRelativePosition(4).bin
    '10'
    >>> createRelativePosition(5).bin
    '11'
    >>> createRelativePosition(6).bin
    '000'
    >>> createRelativePosition(7).bin
    '001'
    >>> createRelativePosition(8).bin
    '010'
    >>> createRelativePosition(9).bin
    '011'
    >>> createRelativePosition(10).bin
    '100'
    >>> createRelativePosition(11).bin
    '101'
    >>> createRelativePosition(12).bin
    '110'
    >>> createRelativePosition(13).bin
    '111'
    >>> createRelativePosition(14).bin
    '0000'
    >>> createRelativePosition(15).bin
    '0001'
    >>> createRelativePosition(16).bin
    '0010'
    >>> createRelativePosition(17).bin
    '0011'
    """
    if (absolutePosition == 0):
        return Bits('0b0')
    if (absolutePosition == 1):
        return Bits('0b1')
    absPosLength     = bitLen(absolutePosition)
    prevOffsetLength = (absPosLength - 1)
    positionOffset   = (2 ** (prevOffsetLength))
    offsetLength     = bitLen((2 ** absPosLength) - (2 ** prevOffsetLength))
    if (positionOffset > absolutePosition):
        raise Exception("Incorrect absolute position ({}) for extra position length={}".format(absolutePosition, offsetLength))
    newPosition    = absolutePosition - positionOffset
    positionLength = offsetLength
    # positionLength = bitLen(newPosition)
    # if (positionLength < offsetLength):
    #    positionLength = offsetLength
    position = Bits(uint=newPosition, length=positionLength)
    return position

def createFormatObject(lengthBits: int, minBits: int) -> dict:
    positionBits       = lengthBits
    valueBits          = lengthBits
    headerBits         = positionBits + valueBits
    startPositionBits  = 0
    endPositionBits    = positionBits
    startValueBits     = 0
    endValueBits       = valueBits
    maxPositionOptions = (2 ** positionBits) - 1
    maxValueOptions    = (2 ** valueBits) - 1
    maxOptionsCombined = (maxPositionOptions * maxValueOptions)
    return {
        'lengthBits':   lengthBits,
        'headerBits':   headerBits,
        'positionBits': positionBits,
        'valueBits':    valueBits,
        'minBits':      minBits,
        'positionBitsRange':  range(startPositionBits, endPositionBits),
        'valueBitsRange':     range(startValueBits, endValueBits),
        'maxPositionOptions': maxPositionOptions,
        'maxValueOptions':    maxValueOptions,
        'maxOptionsCombined': maxOptionsCombined,
    }